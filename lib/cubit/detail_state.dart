part of 'detail_cubit.dart';

abstract class DetailState extends Equatable {
  const DetailState();

  @override
  List<Object> get props => [];
}

class DetailInitial extends DetailState {}

class DetailLoaded extends DetailState {
  final List<Detail>? detail;

  DetailLoaded(this.detail);

  @override
  List<Object> get props => [detail.toString()];
}

class DetailLoadingFailed extends DetailState {
  final String? message;

  DetailLoadingFailed(this.message);

  @override
  List<Object> get props => [message.toString()];
}
