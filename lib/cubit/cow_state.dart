part of 'cow_cubit.dart';

abstract class CowState extends Equatable {
  const CowState();

  @override
  List<Object> get props => [];
}

class CowInitial extends CowState {}

class CowLoaded extends CowState {
  final List<Cow>? cow;

  CowLoaded(this.cow);

  @override
  List<Object> get props => [cow.toString()];
}

class CowLoadingFailed extends CowState {
  final String? message;

  CowLoadingFailed(this.message);

  @override
  List<Object> get props => [message.toString()];
}
