import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:farm_cow/models/models.dart';
import 'package:farm_cow/services/services.dart';

part 'cow_state.dart';

class CowCubit extends Cubit<CowState> {
  CowCubit() : super(CowInitial());

  Future<void> getCow() async {
    ApiReturnValue<List<Cow>> result = await CowServices.getCow();

    if (result.value != null) {
      emit(CowLoaded(result.value));
    } else {
      emit(CowLoadingFailed(result.massage));
    }
  }
}
