import 'dart:io';

import 'package:farm_cow/services/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:farm_cow/models/models.dart';

part 'user_state.dart';

class UserCubit extends Cubit<UserState> {
  UserCubit() : super(UserInitial());

  Future<void> signIn(String email, String password) async {
    ApiReturnValue<User>? result = await UserServices.signIn(email, password);

    if (result!.value != null) {
      emit(UserLoaded(result.value));
    } else {
      emit(UserLoadingFailed(result.massage));
    }
  }

  Future<void> signUp(User user, String password, {File? pictureFile}) async {
    ApiReturnValue<User>? result =
        await UserServices.signUp(user, password, pictureFile: pictureFile);

    if (result!.value != null) {
      emit(UserLoaded(result.value));
    } else {
      emit(UserLoadingFailed(result.massage));
    }
  }

  Future<void> uploadProfilePicture(File pictureFile) async {
    ApiReturnValue<String>? result =
        await UserServices.uploadProfilePicture(pictureFile);

    if (result!.value != null) {
      emit(UserLoaded((state as UserLoaded)
          .user!
          .copyWith(photo_source: "" + result.value.toString())));
      // .copyWith(picturePath: "" + result.value.toString())));
    }
  }

  Future<User?> editData(User user, String password,
      {File? pictureFile}) async {
    ApiReturnValue<User>? result =
        await UserServices.editData(user, password, pictureFile: pictureFile);

    if (result!.value != null) {
      emit(UserLoaded(result.value));
    } else {
      emit(UserLoadingFailed(result.massage));
    }

    // if (result!.value != null) {
    //   emit(UserLoaded((state as UserLoaded).user! + [result.value]));
    //   return result.value;
    // } else {
    //   emit(UserLoadingFailed(result.massage));
    // }
  }
}
