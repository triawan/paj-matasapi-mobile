import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:farm_cow/models/models.dart';
import 'package:farm_cow/services/services.dart';

part 'detail_state.dart';

class DetailCubit extends Cubit<DetailState> {
  DetailCubit() : super(DetailInitial());

  Future<void> getDetail() async {
    ApiReturnValue<List<Detail>> result = await DetailServices.getDetail();

    if (result.value != null) {
      emit(DetailLoaded(result.value));
    } else {
      emit(DetailLoadingFailed(result.massage));
    }
  }

  // Future<bool> submitDetail(Detail detail) async {
  //   ApiReturnValue<Detail> result = await DetailServices.submitDetail(detail);

  //   if (result.value != null) {
  //     emit(DetailLoaded((state as DetailLoaded).detail! + [result.massage]));
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }
}
