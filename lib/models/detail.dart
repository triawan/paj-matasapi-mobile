part of 'models.dart';

class Detail extends Equatable {
  final int? id;
  final Cow? cow;
  final DateTime? dateTime;
  final User? user;

  Detail({this.id, this.cow, this.dateTime, this.user});

  Detail copyWith({int? id, Cow? cow, DateTime? dateTime, User? user}) {
    return Detail(
      id: id ?? this.id,
      cow: cow ?? this.cow,
      dateTime: dateTime ?? this.dateTime,
      user: user ?? this.user,
    );
  }

  @override
  List<Object?> get props => [id, cow, dateTime, user];
}

List<Detail> mockDetail = [
  Detail(
    id: 1,
    cow: mockCows[0],
    dateTime: DateTime.now(),
    user: mockUser,
  ),
  Detail(
    id: 2,
    cow: mockCows[1],
    dateTime: DateTime.now(),
    user: mockUser,
  ),
  Detail(
    id: 3,
    cow: mockCows[3],
    dateTime: DateTime.now(),
    user: mockUser,
  ),
  Detail(
    id: 4,
    cow: mockCows[3],
    dateTime: DateTime.now(),
    user: mockUser,
  ),
  Detail(
    id: 5,
    cow: mockCows[4],
    dateTime: DateTime.now(),
    user: mockUser,
  ),
];
