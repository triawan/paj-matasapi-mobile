part of 'models.dart';

class DataCow extends Equatable {
  final String? totalCows;
  final String? maleCows;
  final String? femaleCows;
  final String? sickCows;
  final String? healtyCows;
  final User? user;

  const DataCow(
      {this.totalCows,
      this.maleCows,
      this.femaleCows,
      this.sickCows,
      this.healtyCows,
      this.user});

  @override
  List<Object?> get props =>
      [totalCows, maleCows, femaleCows, sickCows, healtyCows, user];
}

DataCow mockData = DataCow(
    totalCows: "10",
    maleCows: "5",
    femaleCows: "5",
    sickCows: "1",
    healtyCows: "9",
    user: mockUser);
