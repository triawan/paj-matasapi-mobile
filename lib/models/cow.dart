part of 'models.dart';

enum HealtyStatus { sehat, sakit }

class Cow extends Equatable {
  final int? id;
  final String? picturePath;
  final String? name;
  final String? description;
  final HealtyStatus? status;
  // final List<HealtyStatus>? status;
  final String? atitude;
  final int? suhu;
  final int? udara;
  final int? kelembapan;

  Cow(
      {this.id,
      this.picturePath,
      this.name,
      this.description,
      this.status,
      // this.status = const [],
      this.atitude,
      this.suhu,
      this.udara,
      this.kelembapan});

  @override
  List<Object?> get props => [
        id,
        picturePath,
        name,
        description,
        status,
        atitude,
        suhu,
        udara,
        kelembapan
      ];
}

List<Cow> mockCows = [
  Cow(
    id: 1,
    picturePath:
        "https://www.teahub.io/photos/full/25-259564_cow-weird-face-funny-los-bendrong-siluman-sapi.jpg",
    name: "Sapi 1",
    description: "Sapi paling kuat",
    status:
        // [HealtyStatus.semua, HealtyStatus.sehat],
        HealtyStatus.sehat,
    atitude: "Duduk",
    suhu: 34,
    udara: 6,
    kelembapan: 16,
  ),
  Cow(
    id: 2,
    picturePath:
        "https://panennews.com/wp-content/uploads/2020/01/sapi-limousin.jpg",
    name: "Sapi 2",
    description: "Sapi berbadan besar",
    status:
        // [HealtyStatus.semua, HealtyStatus.sehat],
        HealtyStatus.sehat,
    atitude: "Berdiri",
    suhu: 34,
    udara: 6,
    kelembapan: 16,
  ),
  Cow(
    id: 3,
    picturePath:
        "https://awsimages.detik.net.id/community/media/visual/2021/07/19/sapi-limosin.jpeg?w=724",
    name: "Sapi 3",
    description: "Sapi suka rumput",
    status:
        // [HealtyStatus.semua, HealtyStatus.sakit],
        HealtyStatus.sakit,
    atitude: "Duduk",
    suhu: 34,
    udara: 6,
    kelembapan: 16,
  ),
  Cow(
    id: 4,
    picturePath:
        "https://i.pinimg.com/originals/5d/a6/1e/5da61e0923dfad5ec2ee3bd885cf4516.png",
    name: "Sapi 4",
    description: "Sapi dengan makan paling banyak",
    status:
        // [HealtyStatus.semua, HealtyStatus.sakit],
        HealtyStatus.sakit,
    atitude: "Berdiri",
    suhu: 34,
    udara: 6,
    kelembapan: 16,
  ),
  Cow(
    id: 5,
    picturePath:
        "https://upload.wikimedia.org/wikipedia/commons/0/0c/Cow_female_black_white.jpg",
    name: "Sapi 5",
    description: "Sapi yang suka rebahan",
    status:
        // [HealtyStatus.semua, HealtyStatus.sehat],
        HealtyStatus.sehat,
    atitude: "Rebahan",
    suhu: 34,
    udara: 6,
    kelembapan: 16,
  ),
];
