// ignore_for_file: non_constant_identifier_names

part of 'models.dart';

class User extends Equatable {
  final int? id;
  final String? nik;
  final String? prefix_title;
  final String? first_name;
  final String? middle_name;
  final String? last_name;
  final String? suffix_title;
  final String? phonetic_name;
  final String? gender;
  final String? place_of_birth;
  final String? date_of_birth;
  final String? address;
  final String? contact;
  final String? body_weight;
  final String? body_height;
  final String? blood_type;
  final String? marital_status;
  final String? emergency_name;
  final String? emergency_contact;
  final String? emergency_address;
  final String? family_card;
  final String? insurance;
  final String? bank_account;
  final String? npwp;
  final String? photo_source;
  final String? personal_website;
  final String? primary_email;
  final String? secondary_email;
  final String? password;
  static String? token;

  User(
      {this.id,
      this.nik,
      this.prefix_title,
      this.first_name,
      this.middle_name,
      this.last_name,
      this.suffix_title,
      this.phonetic_name,
      this.gender,
      this.place_of_birth,
      this.date_of_birth,
      this.address,
      this.contact,
      this.body_weight,
      this.body_height,
      this.blood_type,
      this.marital_status,
      this.emergency_name,
      this.emergency_contact,
      this.emergency_address,
      this.family_card,
      this.insurance,
      this.bank_account,
      this.npwp,
      this.photo_source,
      this.personal_website,
      this.primary_email,
      this.secondary_email,
      this.password});

  factory User.fromJson(Map<String, dynamic> data) => User(
        id: data['id'],
        nik: data['nik'],
        prefix_title: data['prefix_title'],
        first_name: data['first_name'],
        middle_name: data['middle_name'],
        last_name: data['last_name'],
        suffix_title: data['suffix_title'],
        phonetic_name: data['phonetic_name'],
        gender: data['gender_id'],
        place_of_birth: data['place_of_birth'],
        date_of_birth: data['date_of_birth'],
        address: data['address_id'],
        contact: data['contact_id'],
        body_weight: data['body_weight'],
        body_height: data['body_height'],
        blood_type: data['blood_type_id'],
        marital_status: data['marital_status_id'],
        emergency_name: data['emergency_name'],
        emergency_contact: data['emergency_contact'],
        emergency_address: data['emergency_address'],
        family_card: data['family_card_id'],
        insurance: data['insurance_id'],
        bank_account: data['bank_account_id'],
        npwp: data['npwp'],
        photo_source: data['photo_source'],
        personal_website: data['personal_website'],
        primary_email: data['email'],
        secondary_email: data['secondary_email'],
        password: data['password'],
      );

  User copyWith(
          {int? id,
          String? nik,
          String? first_name,
          String? middle_name,
          String? last_name,
          String? phonetic_name,
          String? gender,
          String? place_of_birth,
          String? date_of_birth,
          String? address,
          String? contact,
          String? body_weight,
          String? body_height,
          String? blood_type,
          String? marital_status,
          String? emergency_name,
          String? emergency_contact,
          String? emergency_address,
          String? family_card,
          String? insurance,
          String? bank_account,
          String? npwp,
          String? photo_source,
          String? personal_website,
          String? primary_email,
          String? secondary_email,
          String? password}) =>
      User(
        id: id ?? this.id,
        nik: nik ?? this.nik,
        first_name: first_name ?? this.first_name,
        middle_name: middle_name ?? this.middle_name,
        last_name: last_name ?? this.last_name,
        phonetic_name: phonetic_name ?? this.phonetic_name,
        gender: gender ?? this.gender,
        place_of_birth: place_of_birth ?? this.place_of_birth,
        date_of_birth: date_of_birth ?? this.date_of_birth,
        address: address ?? this.address,
        contact: contact ?? this.contact,
        body_weight: body_weight ?? this.body_weight,
        body_height: body_height ?? this.body_height,
        blood_type: blood_type ?? this.blood_type,
        marital_status: marital_status ?? this.marital_status,
        emergency_name: emergency_name ?? this.emergency_name,
        emergency_contact: emergency_contact ?? this.emergency_contact,
        emergency_address: emergency_address ?? this.emergency_address,
        family_card: family_card ?? this.family_card,
        insurance: insurance ?? this.insurance,
        bank_account: bank_account ?? this.bank_account,
        npwp: npwp ?? this.npwp,
        photo_source: photo_source ?? this.photo_source,
        personal_website: personal_website ?? this.personal_website,
        primary_email: primary_email ?? this.primary_email,
        secondary_email: secondary_email ?? this.secondary_email,
        password: password ?? this.password,
      );

  @override
  List<Object?> get props => [
        id,
        nik,
        first_name,
        middle_name,
        last_name,
        phonetic_name,
        gender,
        place_of_birth,
        date_of_birth,
        address,
        contact,
        body_weight,
        body_height,
        blood_type,
        marital_status,
        emergency_name,
        emergency_contact,
        emergency_address,
        family_card,
        insurance,
        bank_account,
        npwp,
        photo_source,
        personal_website,
        primary_email,
        secondary_email,
        password
      ];
}

User mockUser = User(
    id: 1,
    nik: "0232030203",
    first_name: "",
    middle_name: "",
    last_name: "",
    phonetic_name: "",
    gender: "",
    place_of_birth: "",
    date_of_birth: "",
    address: "",
    contact: "",
    body_weight: "",
    body_height: "",
    blood_type: "",
    marital_status: "",
    emergency_name: "",
    emergency_contact: "",
    emergency_address: "",
    family_card: "",
    insurance: "",
    bank_account: "",
    npwp: "",
    photo_source: "",
    personal_website: "",
    primary_email: "",
    secondary_email: "",
    password: "");

// class User extends Equatable {
//   final int? id;
//   final String? name;
//   final String? email;
//   final String? address;
//   final String? houseNumber;
//   final String? phoneNumber;
//   final String? city;
//   final String? picturePath;
//   static String? token;

//   User({
//     this.id,
//     this.name,
//     this.email,
//     this.address,
//     this.houseNumber,
//     this.phoneNumber,
//     this.city,
//     this.picturePath,
//   });

//   factory User.fromJson(Map<String, dynamic> data) => User(
//         id: data['id'],
//         name: data['name'],
//         email: data['email'],
//         address: data['alamat'],
//         houseNumber: data['nomor_rumah'],
//         phoneNumber: data['no_hp'],
//         city: data['kota'],
//         picturePath: data['photo'],
//       );

//   User copyWith({
//     int? id,
//     String? name,
//     String? email,
//     String? address,
//     String? houseNumber,
//     String? phoneNumber,
//     String? city,
//     String? picturePath,
//   }) =>
//       User(
//         id: id ?? this.id,
//         name: name ?? this.name,
//         email: email ?? this.email,
//         address: address ?? this.address,
//         houseNumber: houseNumber ?? this.houseNumber,
//         phoneNumber: phoneNumber ?? this.phoneNumber,
//         city: city ?? this.city,
//         picturePath: picturePath ?? this.picturePath,
//       );

//   @override
//   List<Object?> get props =>
//       [id, name, email, address, houseNumber, phoneNumber, city, picturePath];
// }

// User mockUser = User(
//   id: 1,
//   name: "Jihyo",
//   address: "jalanin aja Berdua",
//   city: "Bandung",
//   houseNumber: "1212",
//   phoneNumber: "081234567890",
//   email: "jihyo@gmail.com",
//   picturePath:
//       "https://www.allkpop.com/upload/2018/11/af_org/09221706/twice-jihyo.jpg",
// );
