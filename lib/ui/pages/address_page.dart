part of 'pages.dart';

class AddressPage extends StatefulWidget {
  final User? user;
  final String? password;
  final File? pictureFile;

  AddressPage(this.user, this.password, this.pictureFile);
  @override
  _AddressPageState createState() => _AddressPageState();
}

class _AddressPageState extends State<AddressPage> {
  TextEditingController contactController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController placeOfBirthController = TextEditingController();
  TextEditingController dateOfBirthController = TextEditingController();
  bool isLoading = false;
  // var _city = <DropdownMenuItem<dynamic>>[];
  // List<String>? cities;
  // String? selectedCity;
  // @override
  // void initState() {
  //   super.initState();
  //   cities = ['Bandung', 'Jakarta', 'Batam', 'Cirebon'];
  //   selectedCity = cities![0];
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GeneralPage(
        title: "Alamat",
        subtitle: "Make sure it's valid",
        onBackButtonPressed: () {
          Get.back();
        },
        child: Column(
          children: [
            Container(
                width: double.infinity,
                margin:
                    EdgeInsets.fromLTRB(defaultMargin, 26, defaultMargin, 6),
                child: Text(
                  "Nomor Hanphone",
                  style: blackFontStyle2,
                )),
            Container(
              width: double.infinity,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Colors.black),
              ),
              child: TextField(
                controller: contactController,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintStyle: greyFontStyle,
                    hintText: "Ketik Nomor Hanphone"),
              ),
            ),
            Container(
                width: double.infinity,
                margin:
                    EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
                child: Text(
                  "Alamat Rumah",
                  style: blackFontStyle2,
                )),
            Container(
              width: double.infinity,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Colors.black),
              ),
              child: TextField(
                controller: addressController,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintStyle: greyFontStyle,
                    hintText: "Ketik Alamat Rumah"),
              ),
            ),
            Container(
                width: double.infinity,
                margin:
                    EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
                child: Text(
                  "Tanggal Lahir",
                  // "Nomor Rumah",
                  style: blackFontStyle2,
                )),
            Container(
              width: double.infinity,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Colors.black),
              ),
              child: TextField(
                controller: dateOfBirthController,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintStyle: greyFontStyle,
                    hintText: "Tanggal Lahir"),
                // "Ketik Nomor Rumah"),
              ),
            ),
            Container(
                width: double.infinity,
                margin:
                    EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
                child: Text(
                  "Kota Lahir",
                  style: blackFontStyle2,
                )),
            Container(
              width: double.infinity,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Colors.black),
              ),
              child: TextField(
                controller: placeOfBirthController,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintStyle: greyFontStyle,
                    hintText: "Kota Lahir"),
                // "Ketik Nomor Rumah"),
              ),
            ),
            // Container(
            //   width: double.infinity,
            //   margin: EdgeInsets.symmetric(horizontal: defaultMargin),
            //   padding: EdgeInsets.symmetric(horizontal: 10),
            //   decoration: BoxDecoration(
            //     borderRadius: BorderRadius.circular(8),
            //     border: Border.all(color: Colors.black),
            //   ),
            //   child: DropdownButton<dynamic>(
            //     value: selectedCity,
            //     hint: Text("Pilih Kota"),
            //     isExpanded: true,
            //     underline: SizedBox(),
            //     items: cities!
            //         .map((e) => DropdownMenuItem(
            //             value: e, child: Text(e, style: blackFontStyle3)))
            //         .toList(),
            //     onChanged: (item) {
            //       setState(() {
            //         selectedCity = item;
            //       });
            //     },
            //   ),
            // ),
            Container(
                width: double.infinity,
                margin: EdgeInsets.only(top: 24),
                height: 45,
                padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                child: (isLoading == true)
                    ? Center(child: loadingIndicator)
                    : ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            elevation: 8,
                            primary: mainColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8))),
                        onPressed: () async {
                          User user = widget.user!.copyWith(
                            contact: contactController.text,
                            // phoneNumber: phoneNumController.text,
                            address: addressController.text,
                            date_of_birth: dateOfBirthController.text,
                            // houseNumber: houseNumController.text,
                            place_of_birth: placeOfBirthController.text,
                            // city: selectedCity,
                          );
                          setState(() {
                            isLoading = true;
                          });

                          await context.read<UserCubit>().signUp(
                              user, widget.password.toString(),
                              pictureFile: widget.pictureFile);
                          UserState state = context.read<UserCubit>().state;
                          if (state is UserLoaded) {
                            context.read<CowCubit>().getCow();
                            context.read<DetailCubit>().getDetail();
                            Get.off(SuccessSignUpPage());
                          } else {
                            Get.snackbar("", "",
                                backgroundColor: "D9435E".toColor(),
                                icon: Icon(
                                  MdiIcons.closeCircleOutline,
                                  color: Colors.white,
                                ),
                                titleText: Text(
                                  "Sign In Failed",
                                  style: GoogleFonts.poppins(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600),
                                ),
                                messageText: Text(
                                  (state as UserLoadingFailed)
                                      .message
                                      .toString(),
                                  style:
                                      GoogleFonts.poppins(color: Colors.white),
                                ));
                            setState(() {
                              isLoading = false;
                            });
                          }
                        },
                        child: Text(
                          "Daftar Sekarang",
                          style: GoogleFonts.poppins(
                              color: Colors.black, fontWeight: FontWeight.w500),
                        ))),
          ],
        ),
      ),
    );
  }
}
