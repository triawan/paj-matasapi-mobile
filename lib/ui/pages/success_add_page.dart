part of 'pages.dart';

class SuccessAddPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: IlustrationPage(
        title: "Data Sudah di Tambahkan",
        subtitle: "Cukup di rumah dan pantau sapimu",
        picturePath: "assets/ilustration/taxi_bull.png",
        buttonTitle1: "Tambah Sapi Lagi",
        buttonTap1: () {
          Get.offAll(AddCowPage());
        },
        buttonTitle2: "Lihat Sapiku",
        buttonTap2: () {
          Get.offAll(MainPage(
            initialPage: 1,
          ));
        },
      ),
    );
  }
}
