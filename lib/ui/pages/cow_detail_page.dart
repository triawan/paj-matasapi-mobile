part of 'pages.dart';

class CowDetailPage extends StatefulWidget {
  final Function? onBackButtonPressed;
  final Detail? detail;

  CowDetailPage({this.onBackButtonPressed, this.detail});
  @override
  _CowDetailPageState createState() => _CowDetailPageState();
}

class _CowDetailPageState extends State<CowDetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            color: mainColor,
          ),
          SafeArea(
              child: Container(
            color: Colors.white,
          )),
          SafeArea(
              child: Container(
            height: 300,
            width: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(
                        widget.detail!.cow!.picturePath.toString()),
                    fit: BoxFit.cover)),
          )),
          SafeArea(
            child: ListView(children: [
              Column(
                children: [
                  ////! Back Button
                  Container(
                    height: 100,
                    padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: GestureDetector(
                        onTap: () {
                          if (widget.onBackButtonPressed != null) {
                            widget.onBackButtonPressed!();
                          }
                        },
                        child: Container(
                          padding: EdgeInsets.all(3),
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: Colors.black12,
                          ),
                          child: Image.asset("assets/back_arrow_white.png"),
                        ),
                      ),
                    ),
                  ),
                  ////! Body
                  Container(
                      margin: EdgeInsets.only(top: 180),
                      padding:
                          EdgeInsets.symmetric(vertical: 20, horizontal: 16),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20)),
                        color: Colors.white,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    widget.detail!.cow!.name.toString(),
                                    style: blackFontStyle1,
                                  ),
                                  SizedBox(
                                    height: 3,
                                  ),
                                  Text(
                                    widget.detail!.cow!.atitude.toString(),
                                    style: blackFontStyle2,
                                  ),
                                ],
                              ),
                              Row(children: [
                                SizedBox(
                                  width: 100,
                                  child: (widget.detail!.cow!.status ==
                                          HealtyStatus.sehat)
                                      ? Container(
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(50),
                                              color: Colors.lightGreen),
                                          width: 50,
                                          height: 50,
                                          child: Text("Sehat",
                                              style: GoogleFonts.poppins()
                                                  .copyWith(
                                                      color: Colors.white,
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.bold)))
                                      : (widget.detail!.cow!.status ==
                                              HealtyStatus.sakit)
                                          ? Container(
                                              alignment: Alignment.center,
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(50),
                                                  color: Colors.red),
                                              width: 50,
                                              height: 50,
                                              child: Text(
                                                "Sakit",
                                                style: GoogleFonts.poppins()
                                                    .copyWith(
                                                        color: Colors.white,
                                                        fontSize: 20,
                                                        fontWeight:
                                                            FontWeight.bold),
                                              ))
                                          : SizedBox(),
                                )
                              ])
                            ],
                          )
                        ],
                      )),
                  Container(
                    alignment: Alignment.topLeft,
                    margin: EdgeInsets.fromLTRB(15, 10, 0, 5),
                    child: Text(
                      "Suhu: " + widget.detail!.cow!.suhu.toString(),
                      style: blackFontStyle1,
                    ),
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    margin: EdgeInsets.fromLTRB(15, 0, 0, 5),
                    child: Text(
                      "Udara: " + widget.detail!.cow!.udara.toString(),
                      style: blackFontStyle1,
                    ),
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    margin: EdgeInsets.fromLTRB(15, 0, 0, 5),
                    child: Text(
                      "Kelembapan: " +
                          widget.detail!.cow!.kelembapan.toString(),
                      style: blackFontStyle1,
                    ),
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    margin: EdgeInsets.fromLTRB(15, 0, 0, 5),
                    child: Text(
                      "Description:",
                      style: blackFontStyle1,
                    ),
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    margin: EdgeInsets.fromLTRB(15, 0, 0, 5),
                    child: Text(
                      widget.detail!.cow!.description.toString(),
                      style: greyFontStyle,
                    ),
                  ),
                ],
              ),
            ]),
          )
        ],
      ),
    );
  }
}
