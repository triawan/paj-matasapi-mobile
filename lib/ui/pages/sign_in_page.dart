part of 'pages.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  TextEditingController emailcontroller = TextEditingController();
  TextEditingController passwordcontroller = TextEditingController();
  bool isLoading = false;
  bool secureText = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        child: ListView(
          children: [
            Column(
              children: [
                Container(
                  width: 275,
                  height: 275,
                  margin: EdgeInsets.only(
                    top: 10,
                  ),
                  padding: EdgeInsets.all(20),
                  child: Image(
                    image: AssetImage("assets/mata_sapi.jpeg"),
                    fit: BoxFit.contain,
                  ),
                ),
                // Container(
                //     width: double.infinity,
                //     margin:
                //         EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
                //     child: Text(
                //       "Email Address",
                //       style: blackFontStyle2,
                //     )),
                Container(
                  width: double.infinity,
                  margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(color: Colors.black),
                  ),
                  child: TextField(
                    controller: emailcontroller,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintStyle: greyFontStyle,
                        hintText: "Ketik email address"),
                  ),
                ),
                // Container(
                //     width: double.infinity,
                //     margin:
                //         EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
                //     child: Text(
                //       "Password",
                //       style: blackFontStyle2,
                //     )),
                Container(
                  width: double.infinity,
                  margin:
                      EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
                  // EdgeInsets.symmetric(horizontal: defaultMargin),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(color: Colors.black),
                  ),
                  child: TextField(
                    obscureText: secureText,
                    controller: passwordcontroller,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintStyle: greyFontStyle,
                        hintText: "Ketik password",
                        suffixIcon: IconButton(
                          icon: Icon(secureText
                              ? Icons.visibility
                              : Icons.visibility_off),
                          onPressed: () {
                            setState(() {
                              secureText = !secureText;
                            });
                          },
                        )),
                  ),
                ),
                Container(
                  alignment: Alignment.centerRight,
                  margin: EdgeInsets.only(right: 26),
                  child: Text(
                    "Lupa Password",
                    style: GoogleFonts.poppins().copyWith(
                      color: Colors.blue,
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(top: 18),
                    height: 45,
                    padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                    child: isLoading
                        ? loadingIndicator
                        : ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                elevation: 8,
                                primary: mainColor,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            onPressed: () async {
                              setState(() {
                                isLoading = true;
                              });

                              await context.read<UserCubit>().signIn(
                                  emailcontroller.text,
                                  passwordcontroller.text);
                              UserState state = context.read<UserCubit>().state;

                              if (state is UserLoaded) {
                                context.read<CowCubit>().getCow();
                                context.read<DetailCubit>().getDetail();
                                Get.to(MainPage());
                              } else {
                                Get.snackbar("", "",
                                    backgroundColor: "D9435E".toColor(),
                                    icon: Icon(
                                      MdiIcons.closeCircleOutline,
                                      color: Colors.white,
                                    ),
                                    titleText: Text(
                                      "Sign In Failed",
                                      style: GoogleFonts.poppins(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w600),
                                    ),
                                    messageText: Text(
                                      (state as UserLoadingFailed)
                                          .message
                                          .toString(),
                                      style: GoogleFonts.poppins(
                                          color: Colors.white),
                                    ));
                                setState(() {
                                  isLoading = false;
                                });
                              }
                            },
                            child: Text(
                              "Masuk",
                              style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500),
                            ))),
                Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(top: 18),
                    height: 45,
                    padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                    child: isLoading
                        ? loadingIndicator
                        : ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                elevation: 8,
                                primary: greyColor,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            onPressed: () {
                              Get.to(SignUpPage());
                            },
                            child: Text(
                              "Buat Akun Baru",
                              style: GoogleFonts.poppins(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500),
                            ))),
                Container(
                  margin: EdgeInsets.only(top: 22),
                  child: Text("Copyright © 2021 CSN Ilmu Computer IPB",
                      // TernaCow Indonesia",
                      style: GoogleFonts.poppins(
                          color: Colors.black, fontWeight: FontWeight.w500)),
                ),
                Container(
                  child: Text(
                    "All Right Reserved",
                    style: GoogleFonts.poppins(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  child: Text("Versi 1.0",
                      style: GoogleFonts.poppins(
                          color: Colors.black, fontWeight: FontWeight.w500)),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
