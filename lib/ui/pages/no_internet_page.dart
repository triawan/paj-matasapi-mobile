part of 'pages.dart';

class NoInternetPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: IlustrationPage(
        title: "Tidak Ada Jaringan!!!",
        subtitle: " Ayo koneksikan jaringanmu sekarang",
        picturePath: "assets/ilustration/taxi_golden_bull.png",
        buttonTitle1: "Memuat Ulang",
        buttonTap1: () {},
      ),
    );
  }
}
