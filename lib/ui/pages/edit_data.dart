part of 'pages.dart';

class EditData extends StatefulWidget {
  final User? user;
  final String? password;
  final File? pictureFile;

  EditData(this.user, this.password, this.pictureFile);

  @override
  _EditDataState createState() => _EditDataState();
}

class _EditDataState extends State<EditData> {
  User? user;
  String? password;
  File? pictureFile;

  late TextEditingController nikController;
  late TextEditingController firstNameController;
  late TextEditingController middleNameController;
  late TextEditingController lastNameController;
  late TextEditingController phoneticNameController;
  late TextEditingController placeOfBirthController;
  late TextEditingController dateOfBirthController;
  late TextEditingController addressController;
  late TextEditingController contactController;
  late TextEditingController bodyWeightController;
  late TextEditingController bodyHeightController;
  late TextEditingController bloodTypeController;
  late TextEditingController maritalStatusController;
  late TextEditingController emergencyNameController;
  late TextEditingController emergencyContactController;
  late TextEditingController emergencyAddressController;
  late TextEditingController familyCardController;
  late TextEditingController insuranceController;
  late TextEditingController banckAccountController;
  late TextEditingController npwpController;
  late TextEditingController personalWebsiteController;
  late TextEditingController primaryEmailController;
  late TextEditingController secondaryEmailController;
  late TextEditingController passwordController;

  List<String>? gender;
  String? selectedGender;
  bool isLoading = false;
  bool secureText = true;

  @override
  void initState() {
    super.initState();
    gender = ['Male', 'Female'];
    selectedGender = gender![0];
    nikController = TextEditingController(
        text: (user?.nik.toString() != 'null')
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .nik
                .toString()
            : "");
    firstNameController = TextEditingController(
        text: (user?.first_name != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .first_name
                .toString()
            : "");
    middleNameController = TextEditingController(
        text: (user?.middle_name != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .middle_name
                .toString()
            : "");
    lastNameController = TextEditingController(
        text: (user?.last_name != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .last_name
                .toString()
            : "");
    phoneticNameController = TextEditingController(
        text: (user?.phonetic_name != 'null')
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .phonetic_name
                .toString()
            : "");
    placeOfBirthController = TextEditingController(
        text: (user?.place_of_birth != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .place_of_birth
                .toString()
            : "");
    dateOfBirthController = TextEditingController(
        text: (user?.date_of_birth != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .date_of_birth
                .toString()
            : "");
    addressController = TextEditingController(
        text: (user?.address != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .address
                .toString()
            : "");
    contactController = TextEditingController(
        text: (user?.contact != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .contact
                .toString()
            : "");
    bodyWeightController = TextEditingController(
        text: (user?.body_weight != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .body_weight
                .toString()
            : "");
    bodyHeightController = TextEditingController(
        text: (user?.body_height != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .body_height
                .toString()
            : "");
    bloodTypeController = TextEditingController(
        text: (user?.blood_type != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .blood_type
                .toString()
            : "");
    maritalStatusController = TextEditingController(
        text: (user?.marital_status != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .marital_status
                .toString()
            : "");
    emergencyNameController = TextEditingController(
        text: (user?.emergency_name != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .emergency_name
                .toString()
            : "");
    emergencyAddressController = TextEditingController(
        text: (user?.emergency_address != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .emergency_address
                .toString()
            : "");
    emergencyContactController = TextEditingController(
        text: (user?.emergency_contact != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .emergency_contact
                .toString()
            : "");
    familyCardController = TextEditingController(
        text: (user?.family_card != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .family_card
                .toString()
            : "");
    insuranceController = TextEditingController(
        text: (user?.insurance != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .insurance
                .toString()
            : "");
    banckAccountController = TextEditingController(
        text: (user?.bank_account != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .bank_account
                .toString()
            : "");
    npwpController = TextEditingController(
        text: (user?.npwp != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .npwp
                .toString()
            : "");
    personalWebsiteController = TextEditingController(
        text: (user?.personal_website != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .personal_website
                .toString()
            : "");
    primaryEmailController = TextEditingController(
        text: (user?.primary_email != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .primary_email
                .toString()
            : "");
    secondaryEmailController = TextEditingController(
        text: (user?.date_of_birth != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .secondary_email
                .toString()
            : "");
    passwordController = TextEditingController(
        text: (user?.password != "null")
            ? (context.read<UserCubit>().state as UserLoaded)
                .user!
                .password
                .toString()
            : "");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Data"),
        backgroundColor: mainColor,
      ),
      body: ListView(
        children: [
          Stack(
            children: [
              Column(
                children: [
                  GestureDetector(
                    onTap: () async {
                      XFile? xFile = await ImagePicker()
                          .pickImage(source: ImageSource.gallery);
                      if (xFile != null) {
                        pictureFile = File(xFile.path);
                        setState(() {});
                      }
                    },
                    child: Container(
                      width: 120,
                      height: 120,
                      margin: EdgeInsets.only(top: 30),
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('assets/photo_border.png'))),
                      child: (pictureFile != null)
                          ? Container(
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: FileImage(pictureFile!),
                                      fit: BoxFit.cover)))
                          : Container(
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: AssetImage('assets/photo.png'),
                                      fit: BoxFit.cover)),
                            ),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "NIK",
                        // "Nama Lengkap",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: nikController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik NIK"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "Nama Depan",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: firstNameController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik Nama Depan"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "Nama Tengah",
                        // "Nama Lengkap",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: middleNameController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik Nama Tengah"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "Nama Terakhir",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: lastNameController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik Nama Terakhir"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "Nama Panggilan",
                        // "Nama Lengkap",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: phoneticNameController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik Nama Panggilan"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 12, defaultMargin, 6),
                      child: Text(
                        "Gender",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: DropdownButton<dynamic>(
                      value: selectedGender,
                      hint: Text("Pilih Gender"),
                      isExpanded: true,
                      underline: SizedBox(),
                      items: gender!
                          .map((e) => DropdownMenuItem(
                              value: e, child: Text(e, style: blackFontStyle3)))
                          .toList(),
                      onChanged: (item) {
                        setState(() {
                          selectedGender = item;
                        });
                      },
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "Kota Lahir",
                        // "Nama Lengkap",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: placeOfBirthController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik Kota Kelahiran"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "Tanggal Lahir",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: dateOfBirthController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik Tanggal Lahir"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "Alamat",
                        // "Nama Lengkap",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: addressController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik Alamat"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "No. Handphone",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: contactController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik No. Handphone"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "Berat Badan",
                        // "Nama Lengkap",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: bodyWeightController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik Berat Badan"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "Tinggi Badan",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: bodyHeightController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik Tinggi Badan"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "Golongan Darah",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: bloodTypeController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik Golongan Darah"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "Status Nikah",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: maritalStatusController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik Status Nikah"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "Nama Darurat",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: emergencyNameController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik Nama Darurat"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "Nomor Darurat",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: emergencyContactController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik Nomor Darurat"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "Alamat Darurat",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: emergencyAddressController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik Alamat Darurat"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "No. Kartu Keluarga",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: familyCardController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik No. Kartu Keluarga"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "No. Asuransi",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: insuranceController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik No. Asuransi"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "No. Bank",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: banckAccountController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik No. Bank"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "NPWP",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: npwpController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik NPWP"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "Website",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: personalWebsiteController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik Website Sendiri"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "Email Utama",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: primaryEmailController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik Email Utama"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "Email Cadangan",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: secondaryEmailController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik Email Cadangan"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 16, defaultMargin, 6),
                      child: Text(
                        "Password",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin:
                        // EdgeInsets.fromLTRB(
                        // defaultMargin, 16, defaultMargin, 6),
                        EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      obscureText: secureText,
                      controller: passwordController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik password",
                          suffixIcon: IconButton(
                            icon: Icon(secureText
                                ? Icons.visibility
                                : Icons.visibility_off),
                            onPressed: () {
                              setState(() {
                                secureText = !secureText;
                              });
                            },
                          )),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(top: 24),
                      height: 45,
                      padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              elevation: 8,
                              primary: Colors.blueAccent,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8))),
                          onPressed: () async {
                            User user = widget.user!.copyWith(
                                  nik: nikController.text,
                                  first_name: firstNameController.text,
                                  middle_name: middleNameController.text,
                                  last_name: lastNameController.text,
                                  phonetic_name: phoneticNameController.text,
                                  gender: selectedGender,
                                  place_of_birth: placeOfBirthController.text,
                                  date_of_birth: dateOfBirthController.text,
                                  address: addressController.text,
                                  contact: contactController.text,
                                  body_weight: bodyWeightController.text,
                                  body_height: bodyHeightController.text,
                                  blood_type: bloodTypeController.text,
                                  marital_status: maritalStatusController.text,
                                  emergency_name: emergencyNameController.text,
                                  emergency_contact:
                                      emergencyContactController.text,
                                  emergency_address:
                                      emergencyAddressController.text,
                                  family_card: familyCardController.text,
                                  insurance: insuranceController.text,
                                  bank_account: banckAccountController.text,
                                  npwp: npwpController.text,
                                  personal_website:
                                      personalWebsiteController.text,
                                  primary_email: primaryEmailController.text,
                                  secondary_email:
                                      secondaryEmailController.text,
                                  password: passwordController.text,
                                ),
                                pictureFile;

                            setState(() {
                              isLoading = true;
                            });

                            await context.read<UserCubit>().editData(
                                user, widget.password.toString(),
                                pictureFile: widget.pictureFile);

                            UserState state = context.read<UserCubit>().state;
                            if (state is UserLoaded) {
                              context.read<CowCubit>().getCow();
                              context.read<DetailCubit>().getDetail();
                              Get.offAll(MainPage(initialPage: 3));
                            } else {
                              Get.snackbar("", "",
                                  backgroundColor: "D9435E".toColor(),
                                  icon: Icon(
                                    MdiIcons.closeCircleOutline,
                                    color: Colors.white,
                                  ),
                                  titleText: Text(
                                    "Sign In Failed",
                                    style: GoogleFonts.poppins(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  messageText: Text(
                                    (state as UserLoadingFailed)
                                        .message
                                        .toString(),
                                    style: GoogleFonts.poppins(
                                        color: Colors.white),
                                  ));
                              setState(() {
                                isLoading = false;
                              });
                            }
                          },
                          child: Text(
                            "Update Profile",
                            style: GoogleFonts.poppins(
                                color: Colors.white,
                                fontWeight: FontWeight.w500),
                          ))),
                  SizedBox(
                    height: 25,
                  ),
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
