part of 'pages.dart';

class AddCowPage extends StatefulWidget {
  final Function? onBackButtonPressed;

  AddCowPage({this.onBackButtonPressed});
  @override
  _AddCowPageState createState() => _AddCowPageState();
}

class _AddCowPageState extends State<AddCowPage> {
  TextEditingController cowDescriptionController = TextEditingController();
  TextEditingController cowNameController = TextEditingController();
  TextEditingController cowGenderController = TextEditingController();
  List<String>? gender;
  String? selectedGender;
  File? pictureFile;

  @override
  void initState() {
    super.initState();
    gender = ['Male', 'Female'];
    selectedGender = gender![0];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Data Sapi"),
        backgroundColor: mainColor,
      ),
      body: ListView(
        children: [
          Stack(
            children: [
              SafeArea(
                  child: Column(
                children: [
                  GestureDetector(
                    onTap: () async {
                      XFile? xFile = await ImagePicker()
                          .pickImage(source: ImageSource.gallery);
                      if (xFile != null) {
                        pictureFile = File(xFile.path);
                        setState(() {});
                      }
                    },
                    child: (pictureFile != null)
                        ? Container(
                            width: 200,
                            height: 200,
                            margin: EdgeInsets.only(top: 10),
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: FileImage(pictureFile!),
                                    fit: BoxFit.contain)),
                          )
                        : Container(
                            width: 120,
                            height: 120,
                            margin: EdgeInsets.only(top: 30),
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image:
                                        AssetImage('assets/photo_border.png'))),
                            child: Container(
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: AssetImage('assets/photo.png'),
                                      fit: BoxFit.cover)),
                            ),
                          ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 12, defaultMargin, 6),
                      child: Text(
                        "Nama Sapi",
                        // "Nama Lengkap",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: cowNameController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik Nama Sapi"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 12, defaultMargin, 6),
                      child: Text(
                        "Jenis Kelamin Sapi",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: DropdownButton<dynamic>(
                      value: selectedGender,
                      hint: Text("Pilih Gender"),
                      isExpanded: true,
                      underline: SizedBox(),
                      items: gender!
                          .map((e) => DropdownMenuItem(
                              value: e, child: Text(e, style: blackFontStyle3)))
                          .toList(),
                      onChanged: (item) {
                        setState(() {
                          selectedGender = item;
                        });
                      },
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.fromLTRB(
                          defaultMargin, 12, defaultMargin, 6),
                      child: Text(
                        "Deskripsi Sapi",
                        // "Nama Lengkap",
                        style: blackFontStyle2,
                      )),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                    ),
                    child: TextField(
                      controller: cowDescriptionController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyFontStyle,
                          hintText: "Ketik Deskripsi Sapi"),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(top: 24),
                      height: 45,
                      padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              elevation: 8,
                              primary: Colors.green,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8))),
                          onPressed: () {
                            Get.offAll(SuccessAddPage());
                          },
                          child: Text(
                            "Save",
                            style: GoogleFonts.poppins(
                                color: Colors.white,
                                fontWeight: FontWeight.w500),
                          ))),
                ],
              ))
            ],
          ),
        ],
      ),
    );
  }
}
