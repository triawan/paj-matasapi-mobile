part of 'pages.dart';

class IlustrationPage extends StatelessWidget {
  final String? title;
  final String? subtitle;
  final String? picturePath;
  final String? buttonTitle1;
  final String? buttonTitle2;
  final VoidCallback? buttonTap1; //'bisa void Function()?'
  final VoidCallback? buttonTap2; //'atau bisa VoidCallback?'

  IlustrationPage({
    @required this.title,
    @required this.subtitle,
    @required this.picturePath,
    @required this.buttonTitle1,
    this.buttonTitle2,
    @required this.buttonTap1,
    this.buttonTap2,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 250,
            height: 250,
            margin: EdgeInsets.only(bottom: 40),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(picturePath!), fit: BoxFit.contain)),
          ),
          Text(
            title!,
            style: blackFontStyle3.copyWith(fontSize: 20),
          ),
          Text(
            subtitle!,
            style: greyFontStyle.copyWith(fontWeight: FontWeight.w300),
            textAlign: TextAlign.center,
          ),
          Container(
            margin: EdgeInsets.only(top: 30, bottom: 12),
            width: 200,
            height: 45,
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    elevation: 8,
                    primary: mainColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8))),
                onPressed: buttonTap1,
                child: Text(
                  buttonTitle1!,
                  style: blackFontStyle3.copyWith(fontWeight: FontWeight.w500),
                )),
          ),
          (buttonTap2 == null)
              ? SizedBox()
              : SizedBox(
                  width: 200,
                  height: 45,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          elevation: 8,
                          primary: '8D92A3'.toColor(),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8))),
                      onPressed: buttonTap2,
                      child: Text(buttonTitle2!,
                          style: blackFontStyle3.copyWith(
                              fontWeight: FontWeight.w500,
                              color: Colors.white))),
                ),
        ],
      ),
    );
  }
}
