part of 'pages.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  User? user;
  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        await context.read<CowCubit>().getCow();
      },
      child: ListView(
        children: [
          Column(
            children: [
              ////* HEADER
              Container(
                padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                color: Colors.white,
                height: 100,
                width: double.infinity,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          (context.read<UserCubit>().state as UserLoaded)
                              .user!
                              .phonetic_name
                              // .name
                              .toString(),
                          style: blackFontStyle1,
                        ),
                        Text(
                          "Let's check the cow",
                          style: greyFontStyle.copyWith(
                              fontWeight: FontWeight.w300),
                        ),
                      ],
                    ),
                    (user?.photo_source != null)
                        ? Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: NetworkImage((context
                                            .read<UserCubit>()
                                            .state as UserLoaded)
                                        .user!
                                        .photo_source
                                        // .picturePath
                                        .toString()),
                                    fit: BoxFit.cover)))
                        : Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image:
                                        AssetImage('assets/unknown_photo.jpg'),
                                    fit: BoxFit.cover)),
                          ),
                  ],
                ),
              ),
              ////* Info Dashboard
              Container(
                height: 210,
                width: double.infinity,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(24, 10, 24, 24),
                  child: DashboardCard(mockData),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
