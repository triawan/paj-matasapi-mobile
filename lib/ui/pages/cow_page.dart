part of 'pages.dart';

class CowPage extends StatefulWidget {
  @override
  _CowPageState createState() => _CowPageState();
}

class _CowPageState extends State<CowPage> {
  int selectedIndex = 0;
  List<Cow> sehat = mockCows
      .where((element) => element.status == HealtyStatus.sehat)
      .toList();
  List<Cow> sakit = mockCows
      .where((element) => element.status == HealtyStatus.sakit)
      .toList();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<CowCubit, CowState>(builder: (_, state) {
        if (state is CowLoaded) {
          if (state.cow!.length == 0) {
            return IlustrationPage(
                title: "Tidak Ada Sapi !!!",
                subtitle:
                    "Tidak sapimu terbang!!!\nAyo tambahkan sapimu dan jaga",
                picturePath: "assets/ilustration/taxi_downloading.png",
                buttonTitle1: "Tambahkan Sapi",
                buttonTap1: () {});
          } else {
            double listItemWidht =
                MediaQuery.of(context).size.width - 2 * defaultMargin;
            return RefreshIndicator(
              onRefresh: () async {
                await context.read<CowCubit>().getCow();
              },
              child: ListView(
                children: [
                  Column(
                    children: [
                      ////* LIST OF COW (TABS)
                      Container(
                        width: double.infinity,
                        color: Colors.white,
                        child: Column(
                          children: [
                            CustomTabbar(
                              titles: ["Semua", "Sehat", "Sakit"],
                              selectedIndex: selectedIndex,
                              onTap: (index) {
                                setState(
                                  () {
                                    selectedIndex = index;
                                  },
                                );
                              },
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            Builder(builder: (_) {
                              List cow = (selectedIndex == 0)
                                  ? state.cow!
                                      .where((element) =>
                                          element.status ==
                                              HealtyStatus.sehat ||
                                          element.status == HealtyStatus.sakit)
                                      .toList()
                                  : (selectedIndex == 1)
                                      ? state.cow!
                                          .where((element) =>
                                              element.status ==
                                              HealtyStatus.sehat)
                                          .toList()
                                      : (selectedIndex == 2)
                                          ? state.cow!
                                              .where((element) =>
                                                  element.status ==
                                                  HealtyStatus.sakit)
                                              .toList()
                                          : [].toList();
                              return Column(
                                children: cow
                                    .map((e) => Padding(
                                          padding: EdgeInsets.fromLTRB(
                                              defaultMargin,
                                              0,
                                              defaultMargin,
                                              16),
                                          child: GestureDetector(
                                            onTap: () {
                                              Get.to(CowDetailPage(
                                                detail: Detail(
                                                    cow: e,
                                                    user: (context
                                                            .read<UserCubit>()
                                                            .state as UserLoaded)
                                                        .user),
                                                onBackButtonPressed: () {
                                                  Get.back();
                                                },
                                              ));
                                            },
                                            child: CowListItem(
                                                cow: e,
                                                itemwidht: listItemWidht),
                                          ),
                                        ))
                                    .toList(),
                              );
                            }),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 60,
                      ),
                    ],
                  )
                ],
              ),
            );
          }
        } else {
          return Center(
            child: loadingIndicator,
          );
        }
      }),
      floatingActionButton: FloatingActionButton(
        backgroundColor: mainColor,
        onPressed: () {
          Get.to(AddCowPage());
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
