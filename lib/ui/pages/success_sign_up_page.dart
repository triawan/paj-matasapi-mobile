part of 'pages.dart';

class SuccessSignUpPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: IlustrationPage(
        title: "Horee!! Selesai",
        subtitle: "Sekarang kamu bisa melihat sapi-sapi kesukaanmu",
        picturePath: "assets/ilustration/taxi_bull_with_flower.png",
        buttonTitle1: "Ayo Cek Sapi",
        buttonTap1: () {
          Get.offAll(SignInPage());
          // Get.off(MainPage(initialPage: 0));
        },
      ),
    );
  }
}
