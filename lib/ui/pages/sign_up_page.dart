part of 'pages.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  User? user;
  File? pictureFile;

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  bool secureText = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GeneralPage(
        title: 'Daftar',
        subtitle: 'Register and check cow',
        onBackButtonPressed: () {
          Get.back();
        },
        child: Column(
          children: [
            GestureDetector(
              onTap: () async {
                XFile? xFile =
                    await ImagePicker().pickImage(source: ImageSource.gallery);
                if (xFile != null) {
                  pictureFile = File(xFile.path);
                  setState(() {});
                }
              },
              child: Container(
                width: 120,
                height: 120,
                margin: EdgeInsets.only(top: 1),
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/photo_border.png'))),
                child: (pictureFile != null)
                    ? Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: FileImage(pictureFile!),
                                fit: BoxFit.cover)))
                    : Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: AssetImage('assets/photo.png'),
                                fit: BoxFit.cover)),
                      ),
              ),
            ),
            Container(
                width: double.infinity,
                margin:
                    EdgeInsets.fromLTRB(defaultMargin, 26, defaultMargin, 6),
                child: Text(
                  "Nama Panggilan",
                  // "Nama Lengkap",
                  style: blackFontStyle2,
                )),
            Container(
              width: double.infinity,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Colors.black),
              ),
              child: TextField(
                controller: nameController,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintStyle: greyFontStyle,
                    hintText: "Ketik Nama Lengkap"),
              ),
            ),
            Container(
                width: double.infinity,
                margin:
                    EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
                child: Text(
                  "Email Address",
                  style: blackFontStyle2,
                )),
            Container(
              width: double.infinity,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Colors.black),
              ),
              child: TextField(
                controller: emailController,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintStyle: greyFontStyle,
                    hintText: "Ketik Email Address"),
              ),
            ),
            Container(
                width: double.infinity,
                margin:
                    EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
                child: Text(
                  "Password",
                  style: blackFontStyle2,
                )),
            Container(
              width: double.infinity,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Colors.black),
              ),
              child: TextField(
                obscureText: secureText,
                controller: passwordController,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintStyle: greyFontStyle,
                    hintText: "Ketik Password",
                    suffixIcon: IconButton(
                      icon: Icon(
                          secureText ? Icons.visibility : Icons.visibility_off),
                      onPressed: () {
                        setState(() {
                          secureText = !secureText;
                        });
                      },
                    )),
              ),
            ),
            Container(
                width: double.infinity,
                margin: EdgeInsets.only(top: 24),
                height: 45,
                padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        elevation: 8,
                        primary: mainColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8))),
                    onPressed: () {
                      Get.to(AddressPage(
                          User(
                              phonetic_name: nameController.text,
                              primary_email: emailController.text),
                          passwordController.text,
                          pictureFile));
                    },
                    child: Text(
                      "Selanjutnya",
                      style: GoogleFonts.poppins(
                          color: Colors.black, fontWeight: FontWeight.w500),
                    ))),
          ],
        ),
      ),
    );
  }
}
