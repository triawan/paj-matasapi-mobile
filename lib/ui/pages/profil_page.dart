part of 'pages.dart';

class ProfilPage extends StatefulWidget {
  @override
  _ProfilPageState createState() => _ProfilPageState();
}

class _ProfilPageState extends State<ProfilPage> {
  int selectedIndex = 0;
  User? user;
  String? password;
  File? pictureFile;

  @override
  Widget build(BuildContext context) {
    return ListView(children: [
      Column(
        children: [
          ////* Foto Profile
          Container(
            width: 120,
            height: 120,
            margin: EdgeInsets.only(top: 26),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/photo_border.png'))),
            child: (user?.photo_source != null)
                ? Container(
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: NetworkImage(
                                (context.read<UserCubit>().state as UserLoaded)
                                    .user!
                                    .photo_source
                                    // .picturePath
                                    .toString()),
                            fit: BoxFit.cover)))
                : Container(
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: AssetImage('assets/unknown_photo.jpg'),
                            fit: BoxFit.cover)),
                  ),
          ),
          ////* Nama dan Email
          Container(
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            margin: EdgeInsets.only(top: 15),
            color: Colors.white,
            height: 60,
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  (context.read<UserCubit>().state as UserLoaded)
                      .user!
                      .phonetic_name
                      // .name
                      .toString(),
                  style: blackFontStyle2,
                ),
                Text(
                  (context.read<UserCubit>().state as UserLoaded)
                      .user!
                      .primary_email
                      // .email
                      .toString(),
                  style: greyFontStyle.copyWith(fontWeight: FontWeight.w300),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 16,
          ),
          ////* Body
          // Container(
          //   width: double.infinity,
          //   color: Colors.white,
          //   child: Column(
          //     children: [
          //       CustomTabbar(
          //         titles: ["Akun", "Pertenakan"],
          //         selectedIndex: selectedIndex,
          //         onTap: (index) {
          //           setState(
          //             () {
          //               selectedIndex = index;
          //             },
          //           );
          //         },
          //       ),
          //       SizedBox(
          //         height: 16,
          //       ),
          //       Column(
          //         children: ((selectedIndex == 0)
          //                 ? [
          //                     'Edit Profile',
          //                     'Alamat Rumah',
          //                     'Keamanan',
          //                     // 'Bantuan',
          //                     // 'Privacy & Policy',
          //                     // 'Term & Condition',
          //                   ]
          //                 : [
          //                     'Edit Lahan',
          //                     'Alamat Pertenakan',
          //                     'Data Sapi',
          //                     'Jumlah Alat'
          //                   ])
          //             .map((e) => Padding(
          //                   padding: EdgeInsets.only(
          //                       bottom: 16,
          //                       left: defaultMargin,
          //                       right: defaultMargin),
          //                   child: GestureDetector(
          //                     onTap: () {},
          //                     child: Row(
          //                       mainAxisAlignment:
          //                           MainAxisAlignment.spaceBetween,
          //                       children: [
          //                         Text(
          //                           e,
          //                           style: blackFontStyle3,
          //                         ),
          //                         SizedBox(
          //                           height: 24,
          //                           width: 24,
          //                           child: Image.asset(
          //                             'assets/right_arrow.png',
          //                             fit: BoxFit.contain,
          //                           ),
          //                         )
          //                       ],
          //                     ),
          //                   ),
          //                 ))
          //             .toList(),
          //       )
          //     ],
          //   ),
          // ),
          GestureDetector(
            onTap: () {
              Get.to(EditData(User(), user?.password, pictureFile));
            },
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: Container(
                height: 50,
                padding: EdgeInsets.symmetric(horizontal: 24),
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    boxShadow: [
                      BoxShadow(
                          spreadRadius: 3,
                          blurRadius: 15,
                          color: Colors.black12)
                    ]),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Edit Profile",
                      style: blackFontStyle3,
                    ),
                    SizedBox(
                      height: 24,
                      width: 24,
                      child: Image.asset(
                        'assets/right_arrow.png',
                        fit: BoxFit.contain,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {},
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: Container(
                height: 50,
                padding: EdgeInsets.symmetric(horizontal: 24),
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    boxShadow: [
                      BoxShadow(
                          spreadRadius: 3,
                          blurRadius: 15,
                          color: Colors.black12)
                    ]),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Lahan Pertenakan",
                      style: blackFontStyle3,
                    ),
                    SizedBox(
                      height: 24,
                      width: 24,
                      child: Image.asset(
                        'assets/right_arrow.png',
                        fit: BoxFit.contain,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {},
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: Container(
                height: 50,
                padding: EdgeInsets.symmetric(horizontal: 24),
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    boxShadow: [
                      BoxShadow(
                          spreadRadius: 3,
                          blurRadius: 15,
                          color: Colors.black12)
                    ]),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Bantuan",
                      style: blackFontStyle3,
                    ),
                    SizedBox(
                      height: 24,
                      width: 24,
                      child: Image.asset(
                        'assets/right_arrow.png',
                        fit: BoxFit.contain,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),

          // Container(
          //     width: 130,
          //     margin: EdgeInsets.only(top: 24),
          //     height: 45,
          //     padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          //     child: ElevatedButton(
          //         style: ElevatedButton.styleFrom(
          //             elevation: 8,
          //             primary: Colors.red,
          //             shape: RoundedRectangleBorder(
          //                 borderRadius: BorderRadius.circular(8))),
          //         onPressed: () {
          //           Get.to(EditData(User(), user?.password, pictureFile));
          //         },
          //         child: Text(
          //           "Edit Profile",
          //           style: GoogleFonts.poppins(
          //               color: Colors.white, fontWeight: FontWeight.w500),
          //         ))),
          // SizedBox(
          //   height: 25,
          // ),
          Container(
              width: 130,
              margin: EdgeInsets.only(top: 24),
              height: 45,
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      elevation: 8,
                      primary: Colors.red,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8))),
                  onPressed: () {
                    Get.to(SignInPage());
                  },
                  child: Text(
                    "Logout",
                    style: GoogleFonts.poppins(
                        color: Colors.white, fontWeight: FontWeight.w500),
                  ))),
          SizedBox(
            height: 10,
          ),
        ],
      )
    ]);
  }
}
