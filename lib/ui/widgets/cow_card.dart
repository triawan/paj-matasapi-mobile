part of 'widget.dart';

class CowCard extends StatelessWidget {
  final Cow cow;

  CowCard(this.cow);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 212,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(spreadRadius: 3, blurRadius: 15, color: Colors.black12)
          ]),
      child: Column(
        children: [
          Container(
            height: 140,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8),
                  topRight: Radius.circular(8),
                ),
                image: DecorationImage(
                    image: NetworkImage(cow.picturePath.toString()),
                    fit: BoxFit.cover)),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(12, 6, 12, 6),
            width: 200,
            child: Text(
              cow.name.toString(),
              style: blackFontStyle2,
              maxLines: 1,
              overflow: TextOverflow.clip,
            ),
          ),
          SizedBox(
              width: 65,
              child: (cow.status == HealtyStatus.sehat)
                  ? Container(
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: Colors.lightGreen),
                      width: 50,
                      height: 25,
                      child: Text("Sehat",
                          style: GoogleFonts.poppins().copyWith(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          )))
                  : (cow.status == HealtyStatus.sakit)
                      ? Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Colors.red),
                          width: 50,
                          height: 25,
                          child: Text("Sakit",
                              style: GoogleFonts.poppins().copyWith(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              )))
                      : SizedBox()),
        ],
      ),
    );
  }
}
