import 'package:farm_cow/models/models.dart';
import 'package:farm_cow/shared/shared.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:supercharged/supercharged.dart';

part 'custom_bottom_navbar.dart';
part 'cow_card.dart';
part 'cow_list_item.dart';
part 'custom_tabbar.dart';
part 'dashboard_card.dart';
