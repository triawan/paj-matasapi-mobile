part of 'widget.dart';

class CowListItem extends StatelessWidget {
  final Cow? cow;
  final double? itemwidht;

  CowListItem({@required this.cow, @required this.itemwidht});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 60,
          height: 60,
          margin: EdgeInsets.only(right: 16),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              image: DecorationImage(
                  image: NetworkImage(cow!.picturePath.toString()),
                  fit: BoxFit.cover)),
        ),
        SizedBox(
          width: itemwidht! - 150, // (60 + 12 + 110)
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                cow!.name.toString(),
                style: blackFontStyle2,
                maxLines: 1,
                overflow: TextOverflow.clip,
              ),
              Text(
                cow!.atitude.toString(),
                style: greyFontStyle.copyWith(fontSize: 13),
              ),
            ],
          ),
        ),
        SizedBox(
            width: 65,
            child: (cow!.status == HealtyStatus.sehat)
                ? Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: Colors.lightGreen),
                    width: 50,
                    height: 25,
                    child: Text("Sehat",
                        style: GoogleFonts.poppins().copyWith(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.bold)))
                : (cow!.status == HealtyStatus.sakit)
                    ? Container(
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: Colors.red),
                        width: 50,
                        height: 25,
                        child: Text(
                          "Sakit",
                          style: GoogleFonts.poppins().copyWith(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.bold),
                        ))
                    : SizedBox())
      ],
    );
  }
}
