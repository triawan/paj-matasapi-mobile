part of 'widget.dart';

class DashboardCard extends StatelessWidget {
  final DataCow dataCow;

  DashboardCard(this.dataCow);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 210,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          boxShadow: [
            BoxShadow(spreadRadius: 3, blurRadius: 15, color: Colors.black12)
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 200,
            child: Padding(
              padding: EdgeInsets.only(
                left: 18,
                top: 15,
                bottom: 13,
              ),
              child: Text("Laporan Pertenakan",
                  style: GoogleFonts.poppins().copyWith(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.bold)),
            ),
          ),
          // Container(
          //   width: 200,
          //   child: Padding(
          //     padding: EdgeInsets.only(
          //       left: 18,
          //       bottom: 5,
          //     ),
          //     child: Text(
          //         "Pemilik Pertenakan : " + dataCow.user!.name.toString(),
          //         style: GoogleFonts.poppins().copyWith(
          //             color: Colors.black,
          //             fontSize: 18,
          //             fontWeight: FontWeight.bold)),
          //   ),
          // ),
          Row(
            children: [
              Padding(
                padding: EdgeInsets.only(left: 18, bottom: 5),
                child: Container(
                    width: 125,
                    child: Text(
                      " Sapi Betina : " + dataCow.femaleCows.toString(),
                      style: blackFontStyle2,
                    )),
              ),
              Padding(
                padding: EdgeInsets.only(
                  left: 18,
                ),
                child: Container(
                    width: 150,
                    child: Text(
                      " Sapi Sehat : " + dataCow.healtyCows.toString(),
                      style: blackFontStyle2,
                    )),
              ),
            ],
          ),
          Row(
            children: [
              Padding(
                padding: EdgeInsets.only(left: 18, bottom: 5),
                child: Container(
                    width: 125,
                    child: Text(
                      " Sapi Jantan : " + dataCow.maleCows.toString(),
                      style: blackFontStyle2,
                    )),
              ),
              Padding(
                padding: EdgeInsets.only(
                  left: 18,
                ),
                child: Container(
                    width: 125,
                    child: Text(
                      " Sapi Sakit : " + dataCow.sickCows.toString(),
                      style: blackFontStyle2,
                    )),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(left: 18),
            child: Container(
                width: 125,
                child: Text(
                  " Total Sapi : " + dataCow.totalCows.toString(),
                  style: blackFontStyle2,
                )),
          ),
        ],
      ),
    );
  }
}
