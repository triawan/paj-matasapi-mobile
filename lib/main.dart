//-------- Tugas UAS -------------
// Praktikum Aplikasi Jaringan  S3 ILKOM IPB------------------

import 'package:farm_cow/cubit/user_cubit.dart';
// ignore: unused_import
import 'package:farm_cow/models/models.dart';
import 'package:farm_cow/ui/pages/pages.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

import 'cubit/cow_cubit.dart';
import 'cubit/detail_cubit.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => UserCubit()),
        BlocProvider(create: (_) => CowCubit()),
        BlocProvider(create: (_) => DetailCubit()),
      ],
      child: GetMaterialApp(
        debugShowCheckedModeBanner: false,
        home:
            // EditData(),
            // AddCowPage(),
            SignInPage(),
      ),
    );
  }
}
