import 'dart:convert';
import 'dart:io';

import 'package:farm_cow/models/models.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

part 'user_services.dart';
part 'cow_services.dart';
part 'detail_services.dart';

String baseURL = 'https://csnipb21.web.id/api/';
