part of 'services.dart';

class CowServices {
  static Future<ApiReturnValue<List<Cow>>> getCow() async {
    await Future.delayed(Duration(milliseconds: 500));
    return ApiReturnValue(value: mockCows);
  }
}
