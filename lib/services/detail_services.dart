part of 'services.dart';

class DetailServices {
  static Future<ApiReturnValue<List<Detail>>> getDetail() async {
    await Future.delayed(Duration(milliseconds: 500));
    return ApiReturnValue(value: mockDetail);
  }

  // static Future<ApiReturnValue<Cow>> getCow() async {
  //   await Future.delayed(Duration(milliseconds: 500));
  //   return ApiReturnValue(value: mockCows);
  // }
}
