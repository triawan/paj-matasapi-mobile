part of 'services.dart';

class UserServices {
  static Future<ApiReturnValue<User>?> signIn(String email, String password,
      {http.Client? client}) async {
    if (client == null) {
      client = http.Client();

      String url = 'https://csnipb21.web.id/api/login';

      var response = await client.post(Uri.parse(url),
          headers: {"Content-Type": "application/json"},
          body: jsonEncode(
              <String, String>{'email': email, 'password': password}));

      if (response.statusCode != 200) {
        return ApiReturnValue(massage: 'Please Try Again');
      }
      var data = jsonDecode(response.body);

      User.token = data['accessToken'];
      User value = User.fromJson(data['user']);

      //   final sharedPref = await SharedPreferences.getInstance();

      // final myMapSpref = json.encode({
      //   'token': User.token,
      //   'id' : User.fromJson(data['user']['id'])
      // });

      // sharedPref.setString('authData', myMapSpref);

      return ApiReturnValue(value: value);
    }

    // http nya hapus jika anonymous
    // await Future.delayed(Duration(microseconds: 500));
    // return ApiReturnValue(value: mockUser);
    // return ApiReturnValue(massage: "Wrong Email or Password");
  }

  static Future<ApiReturnValue<User>?> signUp(User? user, String? password,
      {File? pictureFile, http.Client? client}) async {
    if (client == null) {
      client = http.Client();
    }

    String url = 'https://csnipb21.web.id/api/register';

    print('phonetic_name = ' + user!.phonetic_name.toString());
    print('gender_id = ' + user.gender.toString());
    print('place_of_birth = ' + user.place_of_birth.toString());
    print('date_of_birth = ' + user.date_of_birth.toString());
    print('address_id = ' + user.address.toString());
    print('address_id = ' + user.address.toString());
    print('contact = ' + user.contact.toString());
    print('primary_email = ' + user.primary_email.toString());
    print('password = ' + password.toString());

    var response = await client.post(Uri.parse(url),
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json"
        },
        encoding: Encoding.getByName("utf-8"),
        body: jsonEncode(<String, String>{
          // 'nik': user!.nik.toString(),
          // 'prefix_title': user.prefix_title.toString(),
          // 'first_name': user.first_name.toString(),
          // 'middle_name': user.middle_name.toString(),
          // 'last_name': user.last_name.toString(),
          // 'suffix_title': user.suffix_title.toString(),
          'phonetic_name': user.phonetic_name.toString(),
          'gender_id': user.gender.toString(),
          'place_of_birth': user.place_of_birth.toString(),
          'date_of_birth': user.date_of_birth.toString(),
          'address_id': user.address.toString(),
          'contact_id': user.contact.toString(),
          // 'body_weight': user.body_weight.toString(),
          // 'body_height': user.body_height.toString(),
          // 'blood_type_id': user.blood_type.toString(),
          // 'marital_status_id': user.marital_status.toString(),
          // 'emergency_name': user.emergency_name.toString(),
          // 'emergency_contact': user.emergency_contact.toString(),
          // 'emergency_address': user.address.toString(),
          // 'family_card_id': user.family_card.toString(),
          // 'insurance_id': user.insurance.toString(),
          // 'bank_account_id': user.bank_account.toString(),
          // 'npwp': user.npwp.toString(),
          // 'personal_website': user.personal_website.toString(),
          // 'secondary_email': user.secondary_email.toString(),
          'email': user.primary_email.toString(),
          'password': password.toString(),

          // 'name': user!.name.toString(),
          // 'email': user.email.toString(),
          // 'password': password.toString(),
          // 'password_confirmation': password.toString(),
          // 'alamat': user.address.toString(),
          // 'kota': user.city.toString(),
          // 'nomor_rumah': user.houseNumber.toString(),
          // 'no_hp': user.phoneNumber.toString(),
          // 'photo': user.picturePath.toString(),
        }));
    if (response.statusCode != 200) {
      return ApiReturnValue(massage: 'Please try again');
    }

    print(response.body);
    var data = jsonDecode(response.body);

    User.token = data['access_token'];
    User value = User.fromJson(data['user']);

    // if (pictureFile != null) ;
    // ApiReturnValue<String>? result = await uploadProfilePicture(pictureFile!);
    // if (result!.value != null) {
    //   value = value.copyWith(
    //       photo_source:
    //           "https://csnipb21.web.id/api/user" + result.value.toString());
    //   // picturePath: "" + result.value.toString());
    // }

    return ApiReturnValue(value: value);
  }

  static Future<ApiReturnValue<String>?> uploadProfilePicture(File pictureFile,
      {http.MultipartRequest? request}) async {
    String url = baseURL + 'register';
    var uri = Uri.parse(url);

    if (request == null) {
      request = http.MultipartRequest('POST', uri)
        ..headers["Content-Type"] = "application/json"
        ..headers["Authorization"] = "Bearer ${User.token}";
    }

    var multipartFile =
        await http.MultipartFile.fromPath('file', pictureFile.path);
    request.files.add(multipartFile);

    var response = await request.send();

    if (response.statusCode == 200) {
      String responseBody = await response.stream.bytesToString();
      var data = jsonDecode(responseBody);

      String imagePath = data['data'].toString();

      return ApiReturnValue(value: imagePath);
    } else {
      return ApiReturnValue(massage: 'Upload Profil Picture Failed');
    }
  }

  static Future<ApiReturnValue<User>?> editData(User? user, String? password,
      {File? pictureFile, http.Client? client}) async {
    if (client == null) {
      client = http.Client();
    }
    String url = 'https://csnipb21.web.id/api/user';
    var uri = Uri.parse(url);
    print('contact = ' + user!.contact.toString());

    var response = await client.post(uri,
        headers: {
          "Authorization": "Bearer ${User.token}",
          "Accept": "application/json",
          "Content-Type": "application/json"
        },
        encoding: Encoding.getByName("utf-8"),
        body: jsonEncode(<String, String>{
          'nik': user.nik.toString(),
          'prefix_title': user.prefix_title.toString(),
          'first_name': user.first_name.toString(),
          'middle_name': user.middle_name.toString(),
          'last_name': user.last_name.toString(),
          'suffix_title': user.suffix_title.toString(),
          'phonetic_name': user.phonetic_name.toString(),
          'gender_id': user.gender.toString(),
          'place_of_birth': user.place_of_birth.toString(),
          'date_of_birth': user.date_of_birth.toString(),
          'address_id': user.address.toString(),
          'contact_id': user.contact.toString(),
          'body_weight': user.body_weight.toString(),
          'body_height': user.body_height.toString(),
          'blood_type_id': user.blood_type.toString(),
          'marital_status_id': user.marital_status.toString(),
          'emergency_name': user.emergency_name.toString(),
          'emergency_contact': user.emergency_contact.toString(),
          'emergency_address': user.address.toString(),
          'family_card_id': user.family_card.toString(),
          'insurance_id': user.insurance.toString(),
          'bank_account_id': user.bank_account.toString(),
          'npwp': user.npwp.toString(),
          'personal_website': user.personal_website.toString(),
          'secondary_email': user.secondary_email.toString(),
          'email': user.primary_email.toString(),
          'password': user.password.toString(),

          // 'name': user!.name.toString(),
          // 'email': user.email.toString(),
          // 'password': password.toString(),
          // 'password_confirmation': password.toString(),
          // 'alamat': user.address.toString(),
          // 'kota': user.city.toString(),
          // 'nomor_rumah': user.houseNumber.toString(),
          // 'no_hp': user.phoneNumber.toString(),
          // 'photo': user.picturePath.toString(),
        }));
    if (response.statusCode != 200) {
      return ApiReturnValue(massage: 'Please try again');
    }

    print(response.body);
    var data = jsonDecode(response.body);

    // User.token = data['access_token'];
    User value = User.fromJson(data['user']);

    // if (pictureFile != null) {}
    // ApiReturnValue<String>? result = await uploadProfilePicture(pictureFile!);
    // if (result!.value != null) {
    //   value = value.copyWith(
    //       photo_source:
    //           "https://csnipb21.web.id/api/user" + result.value.toString());
    //   // picturePath: "" + result.value.toString());
    // }

    return ApiReturnValue(value: value);
  }

  // static Future<ApiReturnValue<User>?> logOut() async {
  //   await
  //     }
}
